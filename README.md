# Distractories

The attention economy is a factory for attention-getting distractions: A Distractory

- [Addiction by Design : Machine Gambling in Las Vegas](https://press.princeton.edu/books/paperback/9780691160887/addiction-by-design)
- [The Attention Economy and the Net](https://firstmonday.org/ojs/index.php/fm/article/download/519/440)
- [The Bottleneck of Attention: Connecting Thought With Motivation](https://digitalcollections.library.cmu.edu/node/32562)
- [How Users Reciprocate to Computers: an Experiment That Demonstrates Behavior Change](https://dl.acm.org/doi/10.1145/1120212.1120419)
- [The Scientists Who Make Apps Addictive](https://web.archive.org/web/20210416052902/https://www.economist.com/1843/2016/10/20/the-scientists-who-make-apps-addictive)
