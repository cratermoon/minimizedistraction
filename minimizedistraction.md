# A call to minimize distraction and respect users' attention

by a concerned PM & entrepreneur

[A rough extract of the text from original images]

I'm concerned about how were making the world more distracted.

And my goal with this presentation is to create a movement at Google
to create a new design ethic that aims to minimize distraction, and
I'd like to get your help

Distraction matters to me because time is all we have in life.

Time is expanded and contracted when we're in flow vs. distracted.
You've experienced this before. More can happen in *single day*
travelling in a remote country or in *one hour* of dancing than in
*months* of your work life.

Yet *hours* and *hours* can mysteriously get lost in the email inbox
and feeds that suck *huge chunks of time* away (Facebook scrolling),
weakening our relationships to each other in real life and destroying
our kids' ability to focus (teens 13-17 now send 4,000 texts/month,
*once every 6 minutes awake*)

And today technology companies profoundly influence where all this
attention goes.  In fact, never before in history have the decisions
of a handful of designers (mostly men, white, living in SF, aged
25-35) working at three companies, Google, Apple, and Facebook, had so
much impact on how *millions* of people around the world spend their
attention. Think about that for a second.

We should feel an *enormous* responsibility to get this right.

![Washington Post: 28 percent of accidents involved talking,
texting on cellphones](img/vrg_google_doc_final_vrs03-31.jpg)

We need to be more rigorous about these questions than, "Why don't we
make it buzz your phone every time you get an email?"

Now, you might be saying, "wait a sec, don't users make their *own
choices* here?"

Not always.

1. We need to acknowledge that humans have certain vulnerabilities.
2. Those vulnerabilities can be amplified and exploited
3. And the design of products we make can do this, and make people act
impulsively.

Every day those vulnerabilities make us act against our better
judgement.

## Vulnerability #1: Bad Forecasting

(aka "That won't take long")

![Facebook notification "a friend tagged a photo of
 you"](img/vrg_google_doc_final_vrs03-42.jpg)

What question are we really being asked?

"Want to see this photo of you?" or more like "Do you want to
interrupt what you're doing and spend the next 20 minutes on
Facebook?"

"Share this article?" or  "Make ~26 friends spend 10 minutes reading
this?"

What a related video? or [youtube autoplay list]

- Don't sleep til 2am
- Don't sleep til 3am
- Feel sick tomorrow morning
- Wake up feeling exhausted
- Regret staying up all night watching YouTube

Instead, what if we designed products to help users forecast the
consequences of certain actions?

## Vulnerability #2: Intermittent variable rewards

(aka Slot Machines)

Intermittent (vs predictable) rewards are the most addictive, and
hardest to stop It's why slot machines make the most money at
casinos.

Are we *deciding* to pull for a new email, or do we do it to feel the
intermittent rewards? [Img of phone "Pull for Nothing New (You just
checked 45 seconds ago - srsly?)

Are we swiping two fingers to scroll or playing the ~~slot
machines~~ infinite feeds to see what we'll get?

These are *attention casinos* because the house always wins.  We spend
lots of time – are we getting the same value back?

Instead, what if we designed to *minimize the presence* of
intermittent variable rewards, and *reduce addictions*?

## Vulnerability #3: Loss-Aversion

(aka Fear of Missing Out)

Suppose we actually **wanted to stop** checking all this
stuff. Loss-aversion won't let us, because we'd be **terrified** of
missing *something important*.

So we're forced to live as if every message could be "Hey, nuclear
bomb just exploded over your house. You may need..." instead of "check
out this kitten on rollerblades! LOL", keeping us on a treadmill of
continuous checking.

Instead, what if we designed to give users confidence that they could
disconnect more often, and not miss something important?

## Vulnerability #4: Fast vs Slow Thinking

(aka Mindful vs Mindless behavior)

Humans make different decisions when we *pause and consider* vs when
we react immediately.

When access to the next hit is too frictionless, we lose the ability
to consider before acting. When scrolling frictionless, we don't think
before we flick to see what's next, or when it's so frictionless, we
don't think before we grab our phone after it buzzes, or so
frictionless we don't think before getting a snack after an urge.

When we lose the moment to consider before acting on our impulses, we
lose what sets us apart as thinking humans.

Instead, what if we designed to help users be *fast and efficient*,
while leaving enough friction for users to pause and consider. Just
like Google makes unhealthy food available, but puts them inside jars
and slightly out of sight.  In other words, behind a speed bump.

## Vulnerability #5: Stress & Altered States

(aka "I'm not in the best state of mind to decide")

It's not just how how technology changes *what* we do, it's also how
it changes our *physiological state*.

We actually *stop breathing* when we read our email (an effect known
as "email apnea"). Our sympathetic nervous system is activated,
causing our liver to dump glucose and cholesterol into our blood. Our
heart rate increases and our body prepares for a fight or flight
response.  And all that happens between we read our first email and
when we read our tenth email.

And when we're stressed or distracted we're more likely to succumb to
immediate urges.

Do we really know what we're doing to people?

Instead, what if we designed to minimize stress and create calmer
states of mind?

The problem is, successful products compete by exploiting these
vulnerabilities, so they can't remove them without sacrificing their
success and growth, creating an arms race that causes companies to
find more reasons to steal people's time; a tragedy of the commons
that destroys the common silence and ability to think.

So, what's the solution to all this?

As a former entrepreneur, I can say that niche startups are too small
to tackle this challenge. Change like this can only happen top-down
from large institutions that define the standards for millions of
people.

![](img/vrg_google_doc_final_vrs03-104.jpg)

And we're in a great position to do something about all this. We set
the notification standards on over 50% of the world's mobile phones (4
*trillion* notifications sent last year on iOS alone). We shape over
11 billion interruptions to people's lives every day (this is
nuts!). Millions of knowledge workers spend one third of their day in
our email product. Hundreds of millions live in Chrome every day, and
we have fortunate incentives.

Unlike many companies, our primary business model is fulfilling human
needs (searches), and getting people on with their lives, not stealing
more of your time.

Just like we trust out doctors to do what's best and healthy for us,
who swear by the Hippocratic oath to use their knowledge ethically and
to do no harm, and just like we trust farmers to sell us safe and
healthy food and ask careful questions before making decisions that
could affect our health because all we can do is buy what shows up at
the supermarket.

Consumers trust us to make conscious decisions since we choose the
systems and defaults that shape their lives.  We already care about
speed and productivity, and helping users get shit done.

Let's also care about minimizing distractions and interruptions.

We can design to reduce the volume and frequency of interruptions. We
can design to be respectful about *when* to notify users – let it
wait, unless it's important. We can design to keep users focused, by
putting temptations further away when they're trying to accomplish
goals. We can batch up notifications & messages into digests by
default, instead of one of a time.

Clearly, these are thorny issues. There are tough moral questions,
competition-driven industry pressures, and nuanced human psychology
factors, but just like we had a team to standardize our design
aesthetic across the company, we could have a team to standardize our
design *ethics* and define best practices to minimize distraction.

![fake screen shot of article at The Verge](img/vrg_google_doc_final_vrs03-133.jpg)

But we can't wait any longer to figure it out.

Appeal to join an internal google group and send in the most
distracting aspects of products in your life and ask how would you
change them.

How could we do more to respect the user's attention?

- [Google’s new focus on well-being started five years ago with this presentation](https://www.theverge.com/2018/5/10/17333574/google-android-p-update-tristan-harris-design-ethics)
- [The leader of the Time Well Spent movement has a new crusade](https://www.theverge.com/interface/2019/4/24/18513450/tristan-harris-downgrading-center-humane-tech)
- [Google’s Vision doc for digital wellbeing (deck and transcript)](https://digitalwellbeing.org/googles-internal-digital-wellbeing-presentation-transcript-and-slides/)
